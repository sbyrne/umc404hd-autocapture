On a Linux with ALSA, when a Behringer Uphoria UMC404HD audio interface is plugged in, capture is disabled for the 3rd and 4th inputs.

The udev rules provided here runs a script that enables capture on all of the inputs and sets the volume to 100%.

## Setup:

1. `sudo cp 60-umc404hd-autocapture.rules /etc/udev/rules.d/`
1. `sudo cp umc404hd-autocapture.sh /usr/local/bin/`
1. `udevadm control --reload` (or reboot)

## Issues:

* There may be additional product IDs for the UMC404HD.
* It probably will not work if there are multiple UMC404HD's attached.
